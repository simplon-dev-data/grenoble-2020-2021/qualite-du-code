
import csv # Import CSV.
import re # Import regex.

with open('access.log.txt', 'r') as file:  #Je lis mon jeu de donnée en .log
    with open('accessSQL.csv', 'w', newline='') as csvfile: # Je crée et je vais écrire dans un fichier en .CSV.
        fieldnames = ['adresse_ip','user_identifier','name','date','time_zone','request', 'status', 'size', 'referer', 'user_agent', 'browser', 'système_exploitation', 'appareil', 'autre'] #Titre des tableaux.
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames) # Utilisation des dictionnaires python3.
        writer.writeheader()
        
        # Démarrage recherche jeu de données .log
        for ligne in file:
            matching = re.match(r'^(?P<adresse_ip>\S+) (?P<user_identifier>\S+) (?P<name>\S+) \[(?P<date>[\w:/]+\s)(?P<time_zone>[+-]\d{4})\] "(?P<request>.*)" (?P<status>[0-9]{3}) (?P<size>[0-9]+|-) "(?P<referer>.*)" "(?P<user_agent>.*)" "(?P<autre>.*)"', ligne) # Pour chaque ligne du jeu de données, utilisation d'une regex pour découper la ligne en plusieurs blocs.
            adresse_ip, user_identifier, name, date, time_zone, request, status, size, referer, user_agent, autre = matching.groups() # Création de variables pour chaque groupe matché dans la regex.
            
            # Installation module user-agents permettant de trouver les groupes dans un user-agent. Même principe qu'une regex.
            # Lien du module : https://pypi.org/project/user-agents/
            from user_agents import parse 
            ua_string = user_agent        # ua_string est la variable utilisée pour récupérer le lien de user_agent dans une ligne d'un log.
            user_agent = parse(ua_string) # parse permet de réaliser une recherche générale dans le user_agent (comme une regex).
            browserfamily = str(user_agent.browser.family)  # Cherche le browser du user_agent en question et le récupère.
            systeme_exploitation_family = str(user_agent.os.family) # Cherche le système d'exploitation du user_agent en question et le récupère.
            appareil_family = str(user_agent.device.family) # Cherche l'appareil du user_agent en question et le récupère.
            # Fin du module User_Agent.
            
            writer.writerow({'adresse_ip': adresse_ip, 'user_identifier':user_identifier, 'name':name, 'date':date, 'time_zone':time_zone, 'request':request, 'status':status, 'size':size, 'referer':referer, 'user_agent':user_agent, 'browser':browserfamily, 'système_exploitation':systeme_exploitation_family, 'appareil':appareil_family, 'autre':autre}) # J'écris dans mon fichier.csv, pour chaque colonne, le contenu de la variable qui lui correspond.
             # Fin recherche jeu de donnée.log

