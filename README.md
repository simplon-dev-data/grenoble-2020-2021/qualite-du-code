# Qualité du code

Dans cette activité, nous allons voir quelques règles pour améliorer la qualité d'un code Python. Certaines de ces règles sont issues de la [PEP8](https://www.python.org/dev/peps/pep-0008/).

## Nombre de caractères

Pour des questions de lisibilité, les lignes de code ne doivent pas dépasser les **79 caractères**.

Il est possible de séparer sur plusieurs lignes les élèments contenus entre parenthèses, crochets ou accolades.

Pour une liste, il est intéressant de voir rapidement le nombre d'élèments.

```python
nombres = [45, 48, 1, 56, 156, 12, 14, 14, 15]
# Code plus lisible
nombres = [
    45, 48, 1,
    56, 156, 12,
    14, 14, 15,
]
```

Pour un dictionnaire, il est intéressant de faire ressortir les couples clé/valeur.

```python
params = headers = {'User-Agent': 'My User Agent 1.0','From': 'youremail@domain.com'}
# Code plus lisible
params = {
    'User-Agent': 'My User Agent 1.0',
    'From': 'youremail@domain.com',
}
```

## Règles de nommage

Le choix des noms utilisés en Python (variables, fonctions, modules...) est très important. Ils doivent être explicites.

La PEP8 préconise les règles suivantes :

- pour les modules : noms courts, en minuscules avec des underscores si cela améliore la lisibilité

- pour les fonctions : en minuscules avec des underscores si cela améliore la lisibilité

- pour les variables : en minuscules avec des underscores si cela améliore la lisibilité

- pour les constantes : en majuscules avec des underscores si cela améliore la lisibilité

## Importation de bibliothèques

Les importation de bibliothèques se font en **début** du fichier de code (après les docstrings du module).

L'importation des bibliothèques se fait dans l'ordre suivant : bibliothèque standard, bibliothèques tierces, bibliothèques spécifiques à l'application. Une seule bibliothèque par ligne.

```python
import re
import numpy as np
import mon_module
```

## La documentation

Les docstrings permettent de documenter le fonctionnement des fonctions, des modules. Elles se placent entre triples guillemets (""" ou ''') à la première ligne de la fonction.

```python
def add(a, b):
    """ 
    Add two numbers
    """
    return a+b
```

## Les bibliothèques Python pour vérifier la qualité du code

Il existe différentes bibliothèques Python pour vérifier et améliorer la qualité du code. Voici deux exemples :

- Flake8 : https://flake8.pycqa.org/en/latest/

- Pylint : http://pylint.pycqa.org/en/latest/index.html

VScode permet d'utiliser directement ces bibliothèques dans l'IDE : https://code.visualstudio.com/docs/python/linting

## Activité 

- Améliorer la qualité du code du fichier `exemple_1.py` **sans utiliser** de bibliothèques (Flake8 ou Pylint)

- Utiliser la bibliothèques flake8 sur le fichier `exemple_2.py` pour améliorer la qualité du code (utilisation en ligne de commande ou directement dans l'IDE)

- Utiliser la bibliothèques pylint sur le fichier `exemple_3.py` pour améliorer la qualité du code (utilisation en ligne de commande ou directement dans l'IDE)

## Ressources

- Documentation officielle PEP8 : https://www.python.org/dev/peps/pep-0008/

- Explication de la PEP8 : https://openclassrooms.com/fr/courses/4425111-perfectionnez-vous-en-python/4464230-assimilez-les-bonnes-pratiques-de-la-pep-8

- Ecrire du code Python de qualité : https://openclassrooms.com/fr/courses/6900866-write-maintainable-python-code
