from zipfile import ZipFile
import urllib.request
import re
import csv
from device_detector import DeviceDetector
import requests
import sqlite3
import subprocess


print("Telechargement Zip Log")
url = "https://dataverse.harvard.edu/api/access/datafile/:persistentId?persistentId=doi:10.7910/DVN/3QBYB5/NXKB6J"
urllib.request.urlretrieve(url, 'Access.zip')
print("Fin du DLL")



fichier = "Access.zip"

with ZipFile(fichier, 'r') as unzip:
    unzip.extractall()

print("CSV BRUT")

with open('access.log', 'r') as fichier:  ### OUVERTURE LOG

    with open('access_log.csv', 'w', newline="") as sortie:  #### OUVERTURE POUR ECRIRE DANS UN FORMAT CSV

        try:
            Nom_colone = ['Ip', 'USER', 'TIME', 'Requete', 'REPONSE_STATUS', 'SIZE', 'REFERER', 'USER_AGENT',
                          'A_VISITER', "DETECTION_BOT", " OS_NAME", "TYPE_DEVICE", "NOM_NAVIGATEUR"]
            Ecriture = csv.DictWriter(sortie, fieldnames=Nom_colone)
            Ecriture.writeheader()
            i = 1
            for ligne in fichier:  ###BOUCLE POUR LIGNE DANS FICHIER MATCH POUR ECRIRE LES DONNEES PARSER

                matching = re.search(r'(?P<IP>\S+)'
                                     r' \S+ (?P<user>\S+) '
                                     r'\[(?P<time>.+)\] '
                                     r'"(?P<request>.*)" '
                                     r'(?P<status>[0-9]{3}) '
                                     r'(?P<size>[0-9]+|-) '
                                     r'"(?P<referer>.*)" '
                                     r'"(?P<USERAGENT>.*)" '
                                     r'"(?P<A_VOIR>.*)"', ligne)

                Ip, USER, TIME, Requete, REPONSE_STATUS, SIZE, REFERER, USER_AGENT, A_VISITER = matching.groups()
                device = DeviceDetector(USER_AGENT).parse()

                Ecriture.writerow({'Ip': Ip, 'USER': USER, 'TIME': TIME, 'Requete': Requete,
                                   'REPONSE_STATUS': REPONSE_STATUS,
                                   'SIZE': SIZE, 'REFERER': REFERER, 'USER_AGENT': USER_AGENT, 'A_VISITER': A_VISITER,
                                   "DETECTION_BOT": device.is_bot(), " OS_NAME": device.os_name(),
                                   "TYPE_DEVICE": device.device_type(), "NOM_NAVIGATEUR": device.client_name()})

        except (TypeError, AttributeError):
            print(ligne)

print("Fin CSV BRUT")

print("Transfomation du jeu de donnée")
with open('access_log.csv', mode='r') as csv_file:
    with open('Access_Formater.csv', 'w', newline="") as sortie:  #### OUVERTURE POUR ECRIRE DANS UN FORMAT CSV

        Nom_colone = ['Produit', 'Jour', 'Mois', 'Annee', 'Heure', 'Ip', 'DETECTION_BOT', 'OS_NAME', 'TYPE_DEVICE',
                      'NOM_NAVIGATEUR', 'REFERER']  ###CREATION DES COLONES
        Ecriture = csv.DictWriter(sortie, fieldnames=Nom_colone)
        Ecriture.writeheader()  ####ECRITURE DES COLONES DANS LE CSV
        i = 1

        csv_reader = csv.DictReader(csv_file)

        for row in csv_reader:
            Time = row["TIME"]
            Formatage = row["Requete"]
            Referer_Format = row["REFERER"]
            Ip = row["Ip"]
            Bot = row["DETECTION_BOT"]
            OS = row[" OS_NAME"]
            Device = row["TYPE_DEVICE"]
            Navi_Name = row["NOM_NAVIGATEUR"]


            try:
                Formater_Produit = re.search(r'\w+ (?P<Produit>\/product\/\d+)', Formatage)
                Formater_time = re.search(r'(\d+)\/(\w+)\/(\d+):(\d+)', Time)
                Produit, = Formater_Produit.groups()
                Jour, Mois, Année, Heure = Formater_time.groups()

                Ecriture.writerow({'Produit': Produit, 'Jour': Jour, 'Mois': Mois, 'Annee': Année,
                                   'Heure': Heure, "Ip": Ip, 'DETECTION_BOT': Bot, 'OS_NAME': OS, 'TYPE_DEVICE': Device,
                                   "NOM_NAVIGATEUR": Navi_Name, 'REFERER': Referer_Format})
            except:
              pass


print("Fin Transfomation")

print("Créer BDD")

con = sqlite3.connect("Log_Hadvard.db")
cur = con.cursor()
subprocess.call(["sqlite3", "Log_Hadvard.db",
                 ".mode csv", ".separator ','",
                 ".header on",
                 ".import access_log.csv RAW_BRUT"])
con.commit()
con.close()

con = sqlite3.connect("Log_Hadvard.db")
cur = con.cursor()
subprocess.call(["sqlite3", "Log_Hadvard.db", ".mode csv",
                 ".separator ','",
                 ".header on",
                 ".import Access_Formater.csv Transform"])
con.commit()
con.close()

print("Automatisation OK")

