import sys
import math

def rad(string):
    return math.radians(float(string.replace(',', '.')))

def parse(t):
    return lambda s: dict(map(lambda e: (e[0][1], e[0][0](e[1])), zip(t, s)))
    
def distanceTo(lon, lat):
    def _(d):
        x = (d['lon'] - lon) * math.cos((lat + d['lat'])/2)
        y = d['lat'] - lat
        return x**2 + y**2
    return _

defib_t = [
    (int, 'id'     ),
    (str, 'name'   ),
    (str, 'address'),
    (str, 'phone'  ),
    (rad, 'lon'    ),
    (rad, 'lat'    )
]

lon = rad(input())
lat = rad(input())
n = int(input())
defibs = map(parse(defib_t), (input().split(';') for _ in range(n)))

print(min(defibs, key=distanceTo(lon, lat))['name'])

